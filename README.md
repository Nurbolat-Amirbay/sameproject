# Tamplate for Python project

### Description:
This is a template for a Python web application project

### HOW TO USE
Download hw1.zip. Unzip it. Then execute project_python.exe. Provide name for project and access token.

## Installing development requirements:

```bash
pip install -r requirements.txt
``` 
